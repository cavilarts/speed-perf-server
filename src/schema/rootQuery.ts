import { GraphQLObjectType, GraphQLList, GraphQLSchema, GraphQLNonNull, GraphQLString, GraphQLFloat } from "graphql";

import { ReportType } from './report';
import Report from "../model/report";

const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    reports: {
      type: new GraphQLList(ReportType),
      resolve(parent, args) {
        return Report.find({})
      }
    }
  }
});

const Mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    addReport: {
      type: ReportType,
      args: {
        fistContentfulPaint: {
          type: new GraphQLNonNull(GraphQLString)
        },
        largestContentfulPaint: {
          type: new GraphQLNonNull(GraphQLString)
        },
        timeToInteractive: {
          type: new GraphQLNonNull(GraphQLString)
        },
        totalBlockingTime: {
          type: new GraphQLNonNull(GraphQLString)
        },
        cumulativeLayoutShift: {
          type: new GraphQLNonNull(GraphQLString)
        },
        speedIndex: {
          type: new GraphQLNonNull(GraphQLString)
        },
        score: {
          type: new GraphQLNonNull(GraphQLString)
        },
        time: {
          type: new GraphQLNonNull(GraphQLString)
        },
        site: {
          type: new GraphQLNonNull(GraphQLString)
        },
        device: {
          type: new GraphQLNonNull(GraphQLString)
        }
      },
      resolve(parent, args) {
        const report = new Report({
          fistContentfulPaint: args.fistContentfulPaint,
          largestContentfulPaint: args.largestContentfulPaint,
          timeToInteractive: args.timeToInteractive,
          totalBlockingTime: args.totalBlockingTime,
          cumulativeLayoutShift: args.cumulativeLayoutShift,
          speedIndex: args.speedIndex,
          score: args.score,
          date: args.time,
          site: args.site,
          device: args.device
        })

        return report.save();
      }
    }
  }
});

export default new GraphQLSchema({
  query: RootQuery,
  mutation: Mutation
});