import { GraphQLObjectType, GraphQLID, GraphQLString, GraphQLInt, GraphQLFloat, GraphQLScalarType } from "graphql";

export const ReportType = new GraphQLObjectType({
  name: 'Report',
  fields: () => ({
    id: {
      type: GraphQLID
    },
    fistContentfulPaint: {
      type: GraphQLString
    },
    largestContentfulPaint: {
      type: GraphQLString
    },
    timeToInteractive: {
      type: GraphQLString
    },
    totalBlockingTime: {
      type: GraphQLString
    },
    cumulativeLayoutShift: {
      type: GraphQLString
    },
    speedIndex: {
      type: GraphQLString
    },
    score: {
      type: GraphQLString
    },
    date: {
      type: GraphQLString
    },
    site: {
      type: GraphQLString
    },
    device: {
      type: GraphQLString
    }
  })
});
