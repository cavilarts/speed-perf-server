import { Router } from "express";

import { createReport } from '../../../controller/reportController';

const router = Router();

router.post('/create', createReport);

export default router;