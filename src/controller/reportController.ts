import { reportGenerator } from '../generator/reportGenerator';

export const createReport = async (req:any, res:any) => {
  const {url, times, settings} = req.body;

  const device = settings.device  === 'mobile' || settings.throttling ? {
    auditRefs: [
      {id: 'first-contentful-paint-3g', weight: 0},
    ]
  } : {
    maxWaitForFcp: 15 * 1000,
    maxWaitForLoad: 35 * 1000,
    emulatedFormFactor: 'desktop',
    throttling: {
      rttMs: 40,
      throughputKbps: 10 * 1024,
      cpuSlowdownMultiplier: 1,
      requestLatencyMs: 0,
      downloadThroughputKbps: 0,
      uploadThroughputKbps: 0,
    },
    skipAudits: ['uses-http2'],
  };

  const reportList = [];

  for (let i = 0; i < times; i++) {
    reportList.push(await reportGenerator(device, url));
  }

  res.status(200).json({data: reportList, site: url, device: settings.device});
};