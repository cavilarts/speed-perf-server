import express from 'express';
import graphqlHTTP from 'express-graphql';
import { connect, connection } from 'mongoose';
import cors from 'cors';
import bodyParser from 'body-parser'

import RootQuery from './schema/rootQuery';
import reportRoutes from './router/api/v1/report';

const port = 8080;
const app = express();

app.use(bodyParser.json());
app.use(cors());

connect('mongodb://localhost:27017/reports', { useNewUrlParser: true, useUnifiedTopology: true });
connection.once('open', () => console.log('connected to database'));

app.use('/api/v1/report', reportRoutes);
app.use('/graphql', graphqlHTTP({
  schema: RootQuery,
  graphiql: true
}));

app.listen(port, () => console.log(`app listening at http://localhost:${port}`));