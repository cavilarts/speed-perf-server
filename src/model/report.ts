import { Schema, model } from "mongoose";

const reportSchema = new Schema({
  fistContentfulPaint: String,
  largestContentfulPaint: String,
  timeToInteractive: String,
  totalBlockingTime: String,
  cumulativeLayoutShift: String,
  speedIndex: String,
  score: String,
  date: String,
  site: String,
  device: String
});

export default model('Report', reportSchema);