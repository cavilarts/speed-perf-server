import { launch } from 'chrome-launcher';
import fs from 'fs';

const lighthouse = require('lighthouse');

export const reportGenerator = async (options:any, url:string):Promise<any> => {
  const chrome = await launch({chromeFlags: ['--headless']});
  const mergedOPtions = Object.assign({}, options, {output: 'json', onlyCategories: ['performance'], port: chrome.port });
  const reportResults = await lighthouse(url, mergedOPtions);
  const finalReport = await JSON.parse(reportResults.report);

  await chrome.kill();

  return {
    time: finalReport.fetchTime,
    fistContentfulPaint: finalReport.audits['first-contentful-paint'].displayValue,
    largestContentfulPaint: finalReport.audits['largest-contentful-paint'].displayValue,
    speedIndex: finalReport.audits['speed-index'].displayValue,
    timeToInteractive: finalReport.audits.interactive.displayValue,
    totalBlockingTime: finalReport.audits['total-blocking-time'].displayValue.replace(',', '.'),
    cumulativeLayoutShift: finalReport.audits['cumulative-layout-shift'].displayValue,
    score: finalReport.categories.performance.score
  };
}